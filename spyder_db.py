import os
import cx_Oracle
from flask import Flask, request, render_template, redirect

# Flask 애플리케이션 객체 생성
app = Flask(__name__)

# templates 폴더의 절대 경로 설정
templates_dir = "C:\\test\\templates"
app.template_folder = templates_dir

# Oracle 데이터베이스 연결
conn = cx_Oracle.connect("system", "1234", "localhost:1521/xe")
cursor_oracle = conn.cursor()

# 즐겨찾기 추가 엔드포인트
@app.route('/add_favorite', methods=['POST'])
def add_favorite():
    user_id = request.form.get('user_id')
    movie_id = request.form.get('movie_id')

    # Favorites 테이블에 즐겨찾기 정보 추가
    cursor_oracle.execute('INSERT INTO Favorites (FavoriteID, UserID, MovieID) VALUES (SEQ_FAVORITES.NEXTVAL, :user_id, :movie_id)',
                          {'user_id': user_id, 'movie_id': movie_id})

    # 변경사항 저장 및 연결 종료
    conn.commit()

    return redirect('/movie_info?user_id=' + user_id) # 즐겨찾기 추가 후 영화 정보 페이지로 리다이렉트

# 로그인 엔드포인트
@app.route('/login', methods=['POST'])
def login():
    # 폼에서 제출된 데이터 가져오기
    user_id = request.form.get('user_id')
    password = request.form.get('password')

    # 데이터베이스에서 입력한 아이디와 비밀번호 조회
    cursor_oracle.execute('SELECT * FROM member WHERE UserId = :user_id AND Password = :password',
                          {'user_id': user_id, 'password': password})
    result = cursor_oracle.fetchone()

    # 조회 결과가 있는 경우 로그인 성공
    if result:
        user_name = result[2]  # 사용자 이름을 조회 결과에서 가져옴
        return redirect('/movie_info?user_id=' + user_id)  # 로그인 성공 시 영화 정보 페이지로 이동
    else:
        return '아이디 또는 비밀번호가 잘못되었습니다.'

# 회원가입 폼 보기 엔드포인트
@app.route('/show_form')
def show_form():
    return render_template('db.html')

# 회원가입 엔드포인트
@app.route('/register', methods=['POST'])
def register():
    # 폼에서 제출된 데이터 가져오기
    user_id = request.form.get('user_id')
    password = request.form.get('password')
    name = request.form.get('name')
    favorite_genre = request.form.get('favorite_genre')

    # 데이터베이스에 회원 정보 추가
    cursor_oracle.execute('INSERT INTO member (UserId, Password, Name, FavoriteGenre) VALUES (:user_id, :password, :name, :favorite_genre)',
                          {'user_id': user_id, 'password': password, 'name': name, 'favorite_genre': favorite_genre})

    # 변경사항 저장 및 연결 종료
    conn.commit()

    return render_template('registration_complete.html', user_id=user_id)

# 영화 정보 엔드포인트
@app.route('/movie_info')
def movie_info():
    # 사용자 아이디 받아오기
    user_id = request.args.get('user_id')

    # 데이터베이스에서 영화 정보 조회
    cursor_oracle.execute('SELECT * FROM MoviesInfo')
    movie_records = cursor_oracle.fetchall()

    # 영화 정보를 리스트로 저장
    movies = []
    for record in movie_records:
        movie = {
            'movie_id': record[0],
            'title': record[1],
            'genre': record[2],
            'actor': record[3],
            'director': record[4],
            'plot': record[5],
            'release_year': record[6],
            'release_month': record[7],
            'release_day': record[8],
            'rating': record[9]
        }
        movies.append(movie)

    return render_template('movie_info.html', movies=movies, user_id=user_id)


# 리뷰 작성 엔드포인트
@app.route('/submit_review', methods=['POST'])
def submit_review():
    user_id = request.form.get('user_id')
    movie_id = request.form.get('movie_id')
    review_content = request.form.get('review_content')

    # MovieReview 테이블에 리뷰 정보 추가
    cursor_oracle.execute('INSERT INTO MovieReview (ReviewID, UserId, MovieId, Comm) VALUES (SEQ_MOVIEREVIEW.NEXTVAL, :user_id, :movie_id, :review_content)',
                          {'user_id': user_id, 'movie_id': movie_id, 'review_content': review_content})

    # 변경사항 저장 및 연결 종료
    conn.commit()

    return redirect('/movie_info?user_id=' + user_id) # 리뷰 작성 후 영화 정보 페이지로 리다이렉트

# Flask 애플리케이션 실행
if __name__ == '__main__':
    app.run(debug=True, use_reloader=False, port=8081)